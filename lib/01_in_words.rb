class Fixnum
  ZEROTOTEN = %w(zero one two three four five six seven eight nine)
  ZEROEND = %w(zero ten twenty thirty forty fifty sixty seventy eighty ninety)
  ELEVENABOVE= %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
  THOUSANDABOVE= %w(thousand million billion trillion)

  def number_slice
    number_string = to_s
    slice = []
    number_length = number_string.length
    until number_length - slice.length*3 < 3
      slice = [number_string[-3..-1]] + slice
      number_string = number_string[0..-4]
    end
    slice = [number_string] + slice unless number_string.empty?
    slice
  end
  def in_words_3_digit
    return nil unless to_s.length <= 3 && to_s.length > 0
    if self < 10
      ZEROTOTEN[self]
    elsif self < 20 # within[11..19]
      ELEVENABOVE[self - 10]
    elsif self < 100
      if  (self % 10).zero? # within[10,20,30,...90]
        ZEROEND[self / 10]
      else # other number less than 100
        ZEROEND[self / 10]+" "+ZEROTOTEN[self%10]
      end
    elsif (self % 100).zero?
      (self / 100).in_words_3_digit + " hundred"
    else
      (self / 100).in_words_3_digit + " hundred" + " " + (self % 100).in_words_3_digit
    end
  end

  def in_words
    inword = []
    number_slice.reverse.each_with_index do |el, idx|
      current_word = el.to_i.in_words_3_digit
      if idx.zero?
        inword = current_word unless number_slice.length > 1 && current_word == 'zero'
      elsif current_word != 'zero'
        if inword.empty?
        inword = current_word + " #{THOUSANDABOVE[idx - 1]}"
        else
          inword = current_word + " #{THOUSANDABOVE[idx - 1]} " + inword
        end
      end
    end
    inword
  end

end
